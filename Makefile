all: pngs

pdfs : these.pdf logogua.pdf logomar.pdf logoua.pdf memoire.pdf clean
pngs : these.png logogua.png logomar.png logoua.png memoire.png

.PHONY:%.png
%.png: %.pdf
	pdftoppm $< | pnmtopng > $@
	mkdir -p small
	convert $@ -resize 300x300 small/$@

.PHONY:%.pdf
%.pdf:%.tex uapagedegarde.sty
	latexmk $<

clean :
	rm -f *.log *.aux *.fls *.fdb_latexmk

cleanall: clean
	rm -f *.pdf *.png *~
	rm -rf small/
