# Choisir le type de document

L'utilisation de l'extension se fait simplement par l'ajout dans votre préambule, de la ligne suivante :

    \usepackage[<em>option</em>]{uapagedegarde}

où <em>option</em> prend les valeurs : **these**, **memoire** et **ter** (tout en minuscule).
Pour la réalisation d'une *thèse*, l'on mettra

    \usepackage[these]{uapagedegarde}

# Renseigner les informations de la page de garde

## Titre et date

Pour Renseigner le *titre* et la *date*, il suffit de faire comme avec tout autre document {{< latex />}} :

    \title{Titre\\sur deux lignes}
    \date{date fixe}

## Auteur
La commande **\author** a été modifiée pour identifier le prénom et le nom de famille de l'auteur. En effet en typographie française, le nom de l'auteur doit être écrit en petite capitale. Remarquez que seul la première lettre du nom est en majuscule.

    \author{Prénom}{Nom}

## L'encadrement
Limite est 26 (directeurs) et 1 coencadrant
%------- le(s) directeur(s) et/ou le(s) coencadrant(s)
\directeur{Ray}{Reddington}{agent double}
\coencadrant{Élisabeth}{Keen}{agent du FBI}

## Les commandes spécifiques à la thèse
Les commandes **\rapporteur** et **\jury** ont un effet sur la page de garde uniquement
si vous créer une page de garde de thèse néanmoins elles ne causeront pas d'erreurs pour
tous les autres types de documents.
### Les rapporteurs
Utilisez **\rapporteur** pour renseigner le prénom, nom et qualité de chaque rapporteur.
Utilisez cette commande autant de fois qu'il y a de rapporteurs. Le nombre des rapporteurs est à priori illimité (par construction de l'extension).

    % ------- Les rapporteurs
    \rapporteur{Barth}{Simpson}{fils, Les Simpson}
    \rapporteur{Homer}{Simpson}{Responsable de la sécurité à la centrale nucléaire de Springfield}

### Le jury
Utilisez **\jury** pour renseigner le prénom, nom et qualité de chaque rapporteur.
Utilisez cette commande autant de fois qu'il y a de rapporteurs. Le nombre des rapporteurs est à priori illimité (par construction de l'extension).

    % ------- Le jury
    \jury{Sakura}{Haruno}{Ninja, Konoha}{Examinateur}

## Numéro de thèse
Commande obligatoire. le lien est cliquable

http://www.theses.fr/s\@numerothese


# LuaLaTeX versus pdfLaTeX

Ce qui suit n'est pas *obligatoire* pour compiler la page de garde mais sont incontournable
pour avoir le même rendu.

## Encodage (les accents et autres symboles)
L'un des avantages de LuaLaTeX par rapport à pdfLaTeX est que vous pouvez
utiliser n'importe quel police présente sur votre système mais surtout il n'est
plus nécessaire d'indiquer l'encodage du document par conséquent exit

    \usepackage[utf8]{inputenc}
    \usepackage[T1]{fontenc}


## Les langues

N'utilisez plus *\usepackage[french]{babel}* mais

    \usepackage{polyglossia}
    \setmainlanguage{french}
