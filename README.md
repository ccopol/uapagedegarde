
# Pages de garde <span style="color:#e74c3c;font-size:50px">non </span><span style="color:#e74c3c;font-size:70px">offic</span><span style="color:#e74c3c;font-size:50px">ielles</span> de l'Université des Antilles

Ce dépôt n'a aucun lien avec l'Université des Antilles. C'est un travail personnel pour avoir une page de garde de qualité en
<span style="font-family: Times, 'Times New Roman', serif;letter-spacing: 1px;">L<sup style="  text-transform: uppercase;
  letter-spacing: 1px;
  font-size: 0.85em;
  vertical-align: 0.15em;
  margin-left: -0.36em;
  margin-right: -0.15em;
">a</sup>T<sub style="  text-transform: uppercase;
  vertical-align: -0.5ex;
  margin-left: -0.1667em;
  margin-right: -0.125em;
  font-size: 1em;
">e</sub>X</span>


## Télécharger un aperçu

Télécharger un aperçu de  [these.pdf](https://gitlab.com/ccopol/uapagedegarde/builds/artifacts/master/file/these.pdf?job=uapagedegarde)
ou de [memoire.pdf](https://gitlab.com/ccopol/uapagedegarde/builds/artifacts/master/file/memoire.pdf?job=uapagedegarde)


## Pourquoi ce dépôt
Pour l'instant vous ne trouverez qu'une version pour la thèse avec le tout nouveau logo et respectant l'identité graphique du pôle Guadeloupe. Je compte faire de même pour le pôle Martinique et ensuite ajouter des pages de gardes pour le Master 1 et Master 2.


## Créer la page de garde

### LuaLaTeX
Cette page de garde a été faite pour LuaLaTeX (elle ne fonctionnera pas avec pdfLaTeX). Si vous savez rédiger vos documents pour être compilés avec pdfLaTeX alors vous savez le faire pour LuaLaTeX donc pas de panique. Le changement ne sera pas rude !

##### Pourquoi LuaLaTeX et non pdfLaTeX ?
LuaLaTeX est un peu l'avenir de pdfLaTeX tout comme le fût pdfLaTeX pour LaTeX. L'une des complaintes que l'on peut entendre à propos de pdfLaTeX est l'impossibilité d'utiliser les polices présentes sur sa machine. Ce temps est révolu grâce à LuaLaTeX (entre autre). Un autre avantage est l'utilisation du langage Lua. Rajouter un langage de programmation permet d'automatiser certaines étapes. Imaginer créer un tableau en lisant un fichier de donnée, le code Lua parcoure chaque ligne, rajoute les symboles nécessaire à un tableau LaTeX (& ou \\\\ ...).

# Utilisation<a id="impatients"></a>

### Téléchargement

Télécharger l'archive [uapagedegarde](https://gitlab.com/ccopol/uapagedegarde/builds/artifacts/master/download?job=uapagedegarde) ou cloner/télécharger le dépot. Décompresser l'archive. Vous devriez avoir les fichiers suivants.

    uapagedegarde/
    ├── These.pdf
    ├── These.tex
    ├── uapagedegarde
    │   ├── fonts/
    │   ├── Lamia.pdf
    │   └── UA_logoCMJN_Guadeloupe.jpg
    └── uapagedegarde.sty

Le fichier *These.tex* vous permettra de commencer sans devoir rentrer dans la documentation. Il contient le minimun des extensions à rajouter dans votre fichier.

### Compilation

    lualatex These.tex

### Commandes à renseigner
Comme toutes page de garde de LaTeX vous devez renseigner dans le préambule :

- le titre **\\title**
- l'auteur **\\author**
- la date **\\date**

puis il faut mettre **\\maketitle** après **\\begin{document}**.
Les commandes rajoutées sont :

- **\\directeur**
- **\\coencadrant**
- **\\jury**

Vous ne pouvez mettre qu'un directeur et qu'un coencadrant à ce jour. En revanche, le nombre de juries est théoriquement illimités. Il suffit d'appeler plusieurs fois la commande **\\jury**.

### Fichier .tex standard
Télécharger ici le contenu minimaliste d'un fichier

- [memoire.tex](https://gitlab.com/ccopol/uapagedegarde/builds/artifacts/master/file/memoire.tex?job=uapagedegarde)
- [these.tex](https://gitlab.com/ccopol/uapagedegarde/builds/artifacts/master/file/these.tex?job=uapagedegarde)
